package main

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
)

func (app *application) readJson(res http.ResponseWriter, req *http.Request, data interface{}) error {
	maxBytes := 1048576 // 1MB
	req.Body = http.MaxBytesReader(res, req.Body, int64(maxBytes))

	decoder := json.NewDecoder(req.Body)
	err := decoder.Decode(data)
	if err != nil {
		return err
	}
	// only one json file allowed
	err = decoder.Decode(&struct{}{})
	if err != io.EOF {
		return errors.New("bad request!, body must have only one json value")
	}
	return nil
}

func (app *application) writeJson(res http.ResponseWriter, status int, data interface{}, headers ...http.Header) error {
	out, err := json.Marshal(data)
	if err != nil {
		return err
	}

	if len(headers) > 0 {
		for key, value := range headers[0] {
			res.Header()[key] = value
		}
	}

	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(status)
	_, err = res.Write(out)
	if err != nil {
		return err
	}

	return nil
}

func (app *application) errorJson(res http.ResponseWriter, err error, status ...int) {
	statusCode := http.StatusBadRequest

	if len(status) > 0 {
		statusCode = status[0]
	}

	var payload jsonResponse
	payload.Error = true
	payload.Message = err.Error()

	app.writeJson(res, statusCode, payload)
}
