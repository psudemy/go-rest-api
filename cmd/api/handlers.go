package main

import (
	"net/http"
)

type jsonResponse struct {
	Error   bool   `json:"error"`
	Message string `json:"message"`
}

func (app *application) Login(res http.ResponseWriter, req *http.Request) {
	type credentials struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}

	var creds credentials
	var payload jsonResponse

	err := app.readJson(res, req, &creds)
	if err != nil {
		app.errLog.Println(err)
		payload.Error = true
		payload.Message = "missing or invalid json!"
		_ = app.writeJson(res, http.StatusBadRequest, payload)
	}

	// TODO: authenticate
	app.infoLog.Println(creds.Email, creds.Password)

	// send back response
	payload.Error = false
	payload.Message = "success! Logged in"

	err = app.writeJson(res, http.StatusOK, payload)
	if err != nil {
		app.errLog.Println(err)
	}
}
