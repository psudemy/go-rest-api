package main

import (
	"fmt"
	"go-rest-api/internal/driver"
	"log"
	"net/http"
	"os"
)

// config is the type for application configuration
type config struct {
	port int
}

// application is the type for data we want to share with
// the various parts of our app. we'll share this information
// in  most cases by using this type as the reciever for funcs
type application struct {
	config  config
	infoLog *log.Logger
	errLog  *log.Logger
	db      *driver.DB
}

// application main entrypoint
func main() {
	var cfg config
	cfg.port = 8089

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errLog := log.New(os.Stdout, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	dsn := "host=localhost port=5432 user=postgres password=password dbname=go_rest_api sslmode=disable timezone=UTC connect_timeout=5"

	db, err := driver.DBConnect(dsn)
	if err != nil {
		log.Fatal("Could not connect to the database!!!")
	}

	app := &application{
		config:  cfg,
		infoLog: infoLog,
		errLog:  errLog,
		db:      db,
	}

	err = app.serve()
	if err != nil {
		log.Fatal(err)
	}
}

// start webserver
func (app *application) serve() error {
	app.infoLog.Println("API is listening on port", app.config.port)

	serve := &http.Server{
		Addr:    fmt.Sprintf(":%d", app.config.port),
		Handler: app.routes(),
	}

	return serve.ListenAndServe()
}
