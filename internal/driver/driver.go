package driver

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/jackc/pgconn"
	_ "github.com/jackc/pgx/v4"
	_ "github.com/jackc/pgx/v4/stdlib"
)

type DB struct {
	SQL *sql.DB
}

var DBConnection = &DB{}

const maxOpenDbConnections = 5
const maxIdleConnection = 5
const maxDBLifeTime = 5 * time.Minute

func DBConnect(dsn string) (*DB, error) {
	database, err := sql.Open("pgx", dsn)
	if err != nil {
		return nil, err
	}

	database.SetMaxOpenConns(maxOpenDbConnections)
	database.SetMaxIdleConns(maxIdleConnection)
	database.SetConnMaxLifetime(maxDBLifeTime)

	err = testDBConnection(database)
	if err != nil {
		return nil, err
	}

	DBConnection.SQL = database
	return DBConnection, nil
}

func testDBConnection(database *sql.DB) error {
	err := database.Ping()
	if err != nil {
		fmt.Println("Error", err)
		return err
	}
	fmt.Println("Database pinged successfully!!!")
	return nil
}
